from . import algorithms as algo
from . import transformations as trans

from enum import Enum
import math
import numpy as np

from utils import matrix as mat
from utils import MatSize
from utils import Point

def _add_noise(image, signal_noise, snr, size):
    image = image + np.random.normal(
        0, signal_noise / snr, size=(size.y_dim, size.x_dim))

    return np.float32(image)

def _compute_errors(number_of_errors, quantity, x_errors, y_errors):
    """
    Computes error probability and values of rmse along X- and Y- axis.

    Parameters:
        number_of_errors - number of detected errors,
        quantity - number of performed experiments,
        x_errors - array of recorded errors along X-axis,
        y_errors - array of recorded errors along Y-axis.
    """
    detection_probability = 1 - number_of_errors / quantity
    x_rmse = 0
    y_rmse = 0
    if detection_probability == 0:
        x_rmse = math.nan
        y_rmse = math.nan
    else:
        x_rmse = np.sqrt(np.mean([x ** 2 for x in x_errors if abs(x) <= 2]))
        y_rmse = np.sqrt(np.mean([y ** 2 for y in y_errors if abs(y) <= 2]))

    return detection_probability, x_rmse, y_rmse

def _get_random_rotated(source_image: np.ndarray,
    size: MatSize,
    angle: float,
    scale_factor: float):
    """
    Cuts random fragment from the source_image and rotates it on specified angle.

    Parameters:
        source_image - the original image, 2-dimensional matrix,
        size - required size of the fragments,
        angle - the angle we should rotate our fragments on,
        scale_factor - scaling factor of the image.

    Returns:
        rotated fragment and coordinates of it.
    """

    resize_factor = 2
    source_size = MatSize(x_dim=len(source_image[0]), y_dim=len(source_image))
    # Check if we the source is at least two times bigger than requested size.
    if (source_size.x_dim / resize_factor < size.x_dim
        or source_size.y_dim / resize_factor < size.y_dim):
        raise IndexError()

    # Cut "expanded" fragment.
    new_size = MatSize(x_dim=size.x_dim * resize_factor, y_dim=size.y_dim * resize_factor)
    rand_image, coors = mat.random_fragment(source_image, new_size)

    # Rotate, scale and shift expanded fragment
    # and cut from it a fragment with required dimensions.
    y_shift = np.random.uniform(-0.5, 0.5)
    x_shift = np.random.uniform(-0.5, 0.5)
    rand_image = trans.affine_transform(rand_image, angle, scale_factor, x_shift, y_shift)

    p = Point(x=math.ceil(size.x_dim / resize_factor), y=math.ceil(size.y_dim / resize_factor))
    rand_image = mat.cut_fragment(rand_image, p, size)

    # Return coordinates of the central pixel in cut fragment.
    return rand_image, Point(x=coors.x + size.x_dim + x_shift, y=coors.y + size.y_dim + y_shift)

def _do_subpixel_correction(template, matrix, coors):
    """
    Computes values of subpixel shifts between template and matrix and returns corrected coors.
    """
    dx, dy = algo.gradient_method(template, matrix)

    p = Point(x=coors.x + dx, y=coors.y + dy)

    return p

def compute_gradient_method_errors(source_image, size, dx, dy, quantity=50):
    """
    Computes and returns absolute value of average error for detecting subpixel shift.

    Parameters:
        source_image - numpy 2-dimensional array,
        size - dimensions of fragment to be taken from source_image,
        dx and dy - subpixel shifts along X- and Y-axis respectively,
        quantity - number of experiments to be performed.

    Returns:
        (x_error, y_error) - average errors for detecting
            subpixel shifts along X- and Y-axis respectively
    """
    x_error = 0
    y_error = 0
    for _ in range(quantity):
        rand_image, p = mat.random_fragment(source_image, size)
        shifted_image = trans.subpixel_shift(rand_image, dx, dy)

        # signal_noise = np.std(source_image, axis=(0, 1))
        # shifted_image = shifted_image + np.random.normal(
        #     0, signal_noise / 100, size=(size.y_dim, size.x_dim))
        shifted_image = np.float32(shifted_image)

        dx_comp, dy_comp = algo.gradient_method(rand_image, shifted_image)

        x_error += abs(dx - dx_comp)
        y_error += abs(dy - dy_comp)

    return x_error / quantity, y_error / quantity

class DetectionAlgo(Enum):
    cross_correlation = 0
    sift = 1

def compute_probability(source_image: np.ndarray,
                        size: MatSize,
                        snr: float,
                        subpixel_correction: bool,
                        scale_factor: float = 1,
                        angle: float = 0,
                        quantity: int = 50,
                        detection_algo: DetectionAlgo = DetectionAlgo.cross_correlation):
    """
    Computes and returns detection probability, and rmse for first and second
    dimensions of source image.

    Parameters:
        source_image - 2d numpy array, i. e. main image,
        size - dimensions of fragment to be taken from source_image,
        snr - signal/noise ratio for the image itself and noise to be applied to fragment,
        subpixel_correction - indicates whether or not should we do pixel correction,
        scale_factor - scaling factors, values less than 1 indicate image shrinking,
        angle - angle what image will be rotated to,
        quantity - number of experiments to perform.
    """

    number_of_errors = 0
    x_errors = np.zeros(shape=quantity)
    y_errors = np.zeros(shape=quantity)
    scaled_size = size

    detection_func = None
    if detection_algo == DetectionAlgo.cross_correlation:
        detection_func = algo.cross_correlation_detection
    elif detection_algo == DetectionAlgo.sift:
        detection_func = algo.sift_detection
    else:
        raise Exception("Wrong parameter for detection_algo: {}".format(detection_algo))

    for i in range(quantity):
        # Get random and rotated (if angle is not 0) image fragment.
        rand_image, coors = _get_random_rotated(source_image, size, angle, scale_factor)
        # Compute coordinates without subpixel shifts so we can cut initial fragment later.
        rounded_coors = Point(x=round(coors.x) - size.x_dim // 2, y=round(coors.y) - size.y_dim // 2)

        signal_noise = np.std(rand_image, axis=(0, 1))

        # Add noise to the fragment and try to detect it.
        rand_image = _add_noise(rand_image, signal_noise, snr, scaled_size)
        coors_noisy = detection_func(source_image, rand_image)

        # Find values of subpixel shifts of the found image.
        if subpixel_correction:
            coors_noisy = _do_subpixel_correction(
                mat.cut_fragment(source_image, rounded_coors, size), rand_image, coors_noisy)

        x_errors[i] = coors.x - coors_noisy.x
        y_errors[i] = coors.y - coors_noisy.y
        if abs(x_errors[i]) > 2 or abs(y_errors[i]) > 2:
            number_of_errors += 1

    return _compute_errors(number_of_errors, quantity, x_errors, y_errors)
