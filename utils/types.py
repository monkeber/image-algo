from collections import namedtuple

Point = namedtuple("Point", ["x", "y"])
MatSize = namedtuple("MatSize", ["x_dim", "y_dim"])
