import random as rn
import numpy as np

from .types import *

def cut_fragment(matrix, coors, size):
    """
    Cuts out fragment with specified coordinates of the top-left corner
    with provided dimension lengths.

    Parameters:
        matrix - source image to cut a fragment from,
        coors - coorfinates of the top-left corner of an image,
        size - dimensions of a fragment to cut.

    Returns:
        fragment of the specified matrix
    """

    matrix_copy = matrix[coors.y:coors.y + size.y_dim, coors.x:coors.x + size.x_dim]

    return matrix_copy.copy()

def random_fragment(matrix, size):
    """
    Cuts out random fragment of the provided image with specified
    dimensions.

    Parameters:
        matrix - source image to cut a fragment from,
        size - dimensions of a fragment to cut.

    Returns:
        tuple containing cut fragment and coordinates of its top left corner in the source matrix
    """
    new_size = MatSize(y_dim=len(matrix) - size.y_dim, x_dim=len(matrix[0]) - size.x_dim)

    if new_size.x_dim < 0 or new_size.y_dim < 0:
        return 0

    p = Point(x=rn.randint(0, new_size.x_dim - 1), y=rn.randint(0, new_size.y_dim - 1))

    return cut_fragment(matrix, p, size), p

def emplace(matrix, fragment, top_left=Point(0, 0)):
    """
    Emplaces provided fragment into specified matrix.
    """
    image = matrix.copy()
    y= top_left.y + len(fragment)
    x = top_left.x + len(fragment[0])
    cp = image[top_left.y:top_left.y + len(fragment), top_left.x:top_left.x + len(fragment[0])]
    image[top_left.y:top_left.y + len(fragment), top_left.x:top_left.x + len(fragment[0])] = fragment

    return image
