### How To Run
The project has several dependencies you need to install before running the main app:
- SciPy
- NumPy
- OpenCV (tested on version 4.0)
- Matplotlib

After installing all required packages simply run main.py file