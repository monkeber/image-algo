import cv2
import numpy as np

from math import ceil
from scipy import interpolate

import utils
from utils import MatSize
from utils import Point
from utils import matrix as mat

def affine_transform(source: np.ndarray,
    angle: float,
    scale_factor: float,
    x_shift: float,
    y_shift: float) -> np.ndarray:
    """
    Performs affine transform on the specified array.

    Parameters:
        source - 2-dimensional numpy array, i.e. image to perform the transformation on,
        angle - angle of the rotation, positive values mean counter-clockwise direction,
        scale_factor - scaling factor for the output image, values less than 1
            mean shrinking of the image,
        x_shift - value of pixel shift by X-axis,
        y_shift - value of pixel shift by Y-axis.

    Returns:
        transformed image.
    """

    # For odd sizes floor will give us exactly the central pixel, for even sizes it doesn't
    # matters which pixel we will revolve around.
    center = Point(x=source.shape[1] // 2, y=source.shape[0] // 2)

    rotate_mat = cv2.getRotationMatrix2D(center, angle, scale_factor)

    # Since cv2.getRotationMatrix2D does not provide the way to specify shifting values,
    # we can create another matrix in order to multiply it later with the rotate_mat.
    trans_map = np.float32([[1, 0, x_shift], [0, 1, y_shift]])

    # Matrices in OpenCV documentation for some reason are represented transposed to those
    # in russian-language literature and also lacks some rows/columns, therefore we have to
    # reverse order of multiplication and add a row to rotational matrix.
    rotate_mat = np.append(rotate_mat, [[0, 0, 1]], axis=0)
    affine_mat = np.dot(trans_map, rotate_mat)

    return cv2.warpAffine(source, affine_mat, (source.shape[1], source.shape[0]))

def rotate_image(source, angle):
    """
    Rotates image using affine transformation.

    Parameters:
        source - 2-dimensional numpy array,
        angle - angle of the rotation, positive values mean counter-clockwise direction.

    Returns:
        rotated image as a matrix
    """
    size = MatSize(x_dim=source.shape[1], y_dim=source.shape[0])
    template = np.zeros((size.y_dim * 2, size.x_dim * 2))
    template = utils.emplace(template, source, Point(x=size.x_dim // 2, y=size.y_dim // 2))

    # When the X dimension is even we should correct center point by 1,
    # and when it is odd - by 2.
    pixel_correct = 2 if size.x_dim % 2 else 1
 
    center = Point(x=template.shape[1] // 2 - pixel_correct, y=template.shape[0] // 2)
    rot_mat = cv2.getRotationMatrix2D(center, angle, 1)
    rotated = cv2.warpAffine(template, rot_mat, (template.shape[1], template.shape[0]))

    fragment_coors = Point(x=center.x - ceil(size.x_dim / 2), y = center.y - ceil(size.y_dim / 2))

    return mat.cut_fragment(rotated, fragment_coors, size)

def scale_image(source, scale_factor):
    """
    Changes the scale (as well as its size) of the provided image by scale_factor.

    Parameters:
        source - 2-dimensional numpy array,
        scale_factor - scaling factor for the output image, values less than 1
            mean shrinking of the image.

    Returns:
        scaled (and probably resized) image
    """
    interpolation_method = None
    if (scale_factor <= 1):
        interpolation_method = cv2.INTER_AREA
    else:
        interpolation_method = cv2.INTER_CUBIC

    return cv2.resize(source, None, fx=scale_factor, fy=scale_factor, interpolation=interpolation_method)

def subpixel_shift(image_arr, x_shift, y_shift):
    """
    Performs a subpixel shift on the specified image_arr and computes
    new values using cube interpolation.

    image_arr - a two-dimensional array, should be a numpy array (matrix)
    """
    (y_len, x_len) = image_arr.shape

    interpolated = interpolate.interp2d(range(x_len), range(y_len), image_arr, kind='cubic')

    x_coors = [x + x_shift for x in range(x_len)]
    y_coors = [y + y_shift for y in range(y_len)]

    return interpolated(x_coors, y_coors)

