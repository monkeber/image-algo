import cv2
from matplotlib import pyplot as plt
import matplotlib.patches as patches
import numpy as np

def display(array):
    """
    Displays provided array as a raster.
    """
    plt.imshow(array)
    plt.gray()
    plt.show()

def draw_rectangle(array, x_cor, y_cor, x_dim, y_dim):
    """
    Draws an image from provided array and displays
    a rectangle with specified coordinates.
    """
    fig,ax = plt.subplots(1)
    # Display the image
    ax.imshow(array)
    plt.gray()

    # Create a Rectangle patch
    rect = patches.Rectangle(
        (x_cor,y_cor),x_dim,y_dim,linewidth=1,edgecolor='r',facecolor='none')
    ax.add_patch(rect)
    plt.show()

def read_image(name):
    # Read image and cut a fragment from it.
    source_image = cv2.imread(name, cv2.IMREAD_GRAYSCALE)
    # We are working with OpenCV library, which requires arrays to contain 32-bit floating point type.
    source_image = np.float32(source_image)

    return source_image

def plot2d(array_x, array_y, xlabel='', ylabel='', title='', ylim=None, xlim=None):
    plt.plot(array_x, array_y)

    if ylim is not None:
        plt.ylim(ylim)
    if xlim is not None:
        plt.xlim(xlim)

    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    plt.title(title)

    plt.show()
