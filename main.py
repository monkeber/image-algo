import cv2
import numpy as np
import time

from computation import statistics as st
from computation import transformations as trans

from utils import image as im
from utils import matrix as mat

from utils import MatSize
from utils import Point

def image_detection():
    source_image = im.read_image("map.jpg")

    # Compute probability several times and show charts.
    number_of_runs = 5
    probabilities = [None] * number_of_runs
    x_rmse_arr = [None] * number_of_runs
    y_rmse_arr = [None] * number_of_runs

    upper_bound = 20
    lower_bound = 10
    snr_array = np.linspace(lower_bound, upper_bound, number_of_runs)

    fragment_size = MatSize(x_dim=200, y_dim=200)

    start = time.time()
    for i in range(0, number_of_runs):
        probability, x_rmse, y_rmse = st.compute_probability(source_image,
            fragment_size,
            snr=snr_array[i],
            subpixel_correction=True,
            scale_factor=1,
            angle=10,
            quantity=100,
            detection_algo=st.DetectionAlgo.cross_correlation)
        probabilities[i] = probability
        x_rmse_arr[i] = x_rmse
        y_rmse_arr[i] = y_rmse

    end = time.time()
    print("Running time:", end - start)
    print("Average probability:", np.nanmean(probabilities))
    print("Average RMSE X:", np.nanmean(x_rmse_arr))
    print("Average RMSE Y:", np.nanmean(y_rmse_arr))
    print("STD X:", np.nanstd(x_rmse_arr))
    print("STD Y:", np.nanstd(y_rmse_arr))

    im.plot2d(snr_array,
    probabilities,
        xlabel='SNR',
        ylabel='Probability',
        title='Probability and SNR',
        ylim=(-0.1, 1.1))
    im.plot2d(snr_array,
        x_rmse_arr,
        xlabel='SNR',
        ylabel='RMSE X',
        title='RMSE of X axis and SNR',
        ylim=(0, 3))
    im.plot2d(snr_array,
        y_rmse_arr,
        xlabel='SNR',
        ylabel='RMSE Y',
        title='RMSE of Y axis and SNR',
        ylim=(0, 3))

def affine_transform_test():
    source = im.read_image("racoon.jpg")
    transformed = trans.affine_transform(source, 0, 1, 0, 0)
    im.display(transformed)

def rotate_image_test():
    source = im.read_image("racoon.jpg")
    size = MatSize(x_dim=500, y_dim=500)
    size_expanded = MatSize(x_dim=size.x_dim * 2, y_dim=size.y_dim * 2)
    rand_image, p = mat.random_fragment(source, size_expanded)

    rotated = trans.rotate_image(rand_image, 90)
    rotated = mat.cut_fragment(rotated, Point(x=size.x_dim // 2, y=size.y_dim // 2), size)
    im.display(rotated)

def subpixel_method_test():
    source_image = im.read_image("map100x100.jpg")

    x_shift = 0.4
    y_shift = -0.3
    size = MatSize(x_dim=20, y_dim=20)
    experiments_number = 150

    x_aver, y_aver = st.compute_gradient_method_errors(
        source_image, size, x_shift, y_shift, experiments_number)

    print("Fragment size along X-axis:", size.x_dim)
    print("Fragment size along Y-axis:", size.y_dim)
    print("Subpixel shift along X-axis:", x_shift)
    print("Subpixel shift along Y-axis:", y_shift)
    print("Number of experiments:", experiments_number)
    print("Average error along X-axis:", x_aver)
    print("Average error along Y-axis:", y_aver)

if __name__ == "__main__":
    image_detection()
