import numpy as np
import cv2

from utils import Point
from utils import image as im

def corr_coordinates(matrix):
    """
    Finds coordinates of the maximum element in matrix and converts them
    according to the top-left corner of the provided fragment.
    """

    y, x = np.unravel_index(np.argmax(matrix), matrix.shape)

    return Point(x=x, y=y)

def cross_correlation(source, fragment):
    """
    Returns result of cross-correlation of source image and its fragment.

    source and fragment should be 2-dimensional numpy arrays
    """
    matrix = cv2.matchTemplate(image=source, templ=fragment, method=cv2.TM_CCORR_NORMED)

    return matrix

def cross_correlation_detection(source, fragment):
    """
    Performs cross correlation and returns coordinates
    of supposed coordinates of fragment in source.

    Returns estimated coordinates of the central pixel of fragment in source.
    """
    matrix = cross_correlation(source, fragment)
    coors = corr_coordinates(matrix)
    y_len, x_len = fragment.shape

    return Point(x=coors.x + x_len / 2, y=coors.y + y_len / 2)

def filter_matrix(source, mask):
    """
    Applies provided mask to the provided source matrix using filter2d function from OpenCV.
    """
    return cv2.filter2D(src=source, ddepth=-1, kernel=mask)

def gradient_method(matrix, fragment):
    """
    Computes subpixel shifts across X- and Y- axis using gradient method.

    matrix - numpy 2 dimensional array
    fragment - numpy 2 dimensional array
    """
    # Compute image derivatives across X- and Y- axis.
    mask = np.array([[1/12, -8/12, 0, 8/12, -1/12]])
    x_grad = filter_matrix(matrix, mask)
    y_grad = filter_matrix(matrix, mask.T)

    # Compute left matrix for the equation from the article.
    mat_left = np.zeros((2, 2))
    mat_left[0, 0] = np.sum(x_grad ** 2)
    mat_left[1, 1] = np.sum(y_grad ** 2)
    mat_left[1, 0] = np.sum(x_grad * y_grad)
    mat_left[0, 1] = mat_left[1, 0]

    # Compute right matrix for the equation from the article.
    mat_right = np.zeros((2, 1))
    subtraction = fragment - matrix
    mat_right[0, 0] = np.sum(subtraction * x_grad)
    mat_right[1, 0] = np.sum(subtraction * y_grad)

    dxdy = np.dot(np.linalg.inv(mat_left), mat_right)
    dx = dxdy[0, 0]
    dy = dxdy[1, 0]

    return dx, dy

def _calculate_central_point(first: Point, second: Point):
    """
    Calculates the central point on a line drawn from the first to the second point.
    Returns:
        Point - calculated coordinates.
    """
    return Point(x=(first.x + second.x) / 2, y=(first.y + second.y) / 2)

def sift_detection(matrix: np.ndarray, fragment: np.ndarray):
    """
    Uses SIFT keypoints creation and matches them
    between source image and the provided fragment.
    """
    sift = cv2.xfeatures2d.SIFT_create()
    matrix_kp, matrix_desc = sift.detectAndCompute(matrix.astype(np.uint8), None)
    fragment_kp, fragment_desc = sift.detectAndCompute(fragment.astype(np.uint8), None)

    if matrix_desc is None or fragment_desc is None:
        print('Couldn\'t calculate descriptors for matrix or fragment')
        return Point(x=0, y=0)

    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)
    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(fragment_desc, matrix_desc, k=2)

    # store all the good matches as per Lowe's ratio test.
    good = []
    for m,n in matches:
        if m.distance < 0.7 * n.distance:
            good.append(m)

    MIN_MATCH_COUNT = 10
    if len(good)>MIN_MATCH_COUNT:
        src_pts = np.float32([ fragment_kp[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ matrix_kp[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
        matchesMask = mask.ravel().tolist()
        h,w = fragment.shape
        pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
        dst = cv2.perspectiveTransform(pts,M)
    else:
        print("Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT))
        return Point(x=0, y=0)

    # Assuming that calculated dst matrix represents a parallelogram.
    return _calculate_central_point(
        Point(x=dst[0][0][0], y=dst[0][0][1]), Point(x=dst[2][0][0], y=dst[2][0][1]))
